                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              /* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Davide Magrin <magrinda@dei.unipd.it>
 */

#include "ns3/simple-gateway-lora-phy.h"
#include "ns3/lora-tag.h"
#include "ns3/simulator.h"
#include "ns3/log.h"

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("SimpleGatewayLoraPhy");

NS_OBJECT_ENSURE_REGISTERED (SimpleGatewayLoraPhy);

/***********************************************************************
 *                 Implementation of Gateway methods                   *
 ***********************************************************************/

TypeId
SimpleGatewayLoraPhy::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::SimpleGatewayLoraPhy")
                          .SetParent<GatewayLoraPhy> ()
                          .SetGroupName ("lorawan")
                          .AddConstructor<SimpleGatewayLoraPhy> ();

  return tid;
}
SimpleGatewayLoraPhy::CaptureState SimpleGatewayLoraPhy::captureState = SimpleGatewayLoraPhy::MAX;
//NS_LOG_DEBUG(SimpleGatewayLoraPhy::captureState);
SimpleGatewayLoraPhy::SimpleGatewayLoraPhy ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

SimpleGatewayLoraPhy::~SimpleGatewayLoraPhy ()
{
  NS_LOG_FUNCTION_NOARGS ();
}

void
SimpleGatewayLoraPhy::Send (Ptr<Packet> packet, LoraTxParameters txParams, double frequencyMHz,
                            double txPowerDbm)
{
  NS_LOG_FUNCTION (this << packet << frequencyMHz << txPowerDbm);

  // Get the time a packet with these parameters will take to be transmitted
  Time duration = GetOnAirTime (packet, txParams);

  NS_LOG_DEBUG ("Duration of packet: " << duration << ", SF" << unsigned (txParams.sf));

  // Interrupt all receive operations
  std::list<Ptr<SimpleGatewayLoraPhy::ReceptionPath>>::iterator it;
  for (it = m_receptionPaths.begin (); it != m_receptionPaths.end (); ++it)
    {

      Ptr<SimpleGatewayLoraPhy::ReceptionPath> currentPath = *it;

      if (!currentPath->IsAvailable ()) // Reception path is occupied
        {
          // Call the callback for reception interrupted by transmission
          // Fire the trace source
          if (m_device)
            {
              m_noReceptionBecauseTransmitting (currentPath->GetEvent ()->GetPacket (),
                                                m_device->GetNode ()->GetId ());
            }
          else
            {
              m_noReceptionBecauseTransmitting (currentPath->GetEvent ()->GetPacket (), 0);
            }

          // Cancel the scheduled EndReceive call
          Simulator::Cancel (currentPath->GetEndReceive ());

          // Free it
          // This also resets all parameters like packet and endReceive call
          currentPath->Free ();
        }
    }

  // Send the packet in the channel
  m_channel->Send (this, packet, txPowerDbm, txParams, duration, frequencyMHz);

  Simulator::Schedule (duration, &SimpleGatewayLoraPhy::TxFinished, this, packet);

  m_isTransmitting = true;

  // Fire the trace source
  if (m_device)
    {
      m_startSending (packet, m_device->GetNode ()->GetId ());
    }
  else
    {
      m_startSending (packet, 0);
    }
}

void
SimpleGatewayLoraPhy::StartReceive (Ptr<Packet> packet, double rxPowerDbm, uint8_t sf,
                                    Time duration, double frequencyMHz)
{
  NS_LOG_FUNCTION (this << packet << rxPowerDbm << duration << frequencyMHz);

  // Fire the trace source
  m_phyRxBeginTrace (packet);

  if (m_isTransmitting)
    {
      // If we get to this point, there are no demodulators we can use
      NS_LOG_INFO ("Dropping packet reception of packet with sf = "
                   << unsigned (sf) << " because we are in TX mode");

      m_phyRxEndTrace (packet);

      // Fire the trace source
      if (m_device)
        {
          m_noReceptionBecauseTransmitting (packet, m_device->GetNode ()->GetId ());
        }
      else
        {
          m_noReceptionBecauseTransmitting (packet, 0);
        }

      return;
    }

  // Add the event to the LoraInterferenceHelper
  Ptr<LoraInterferenceHelper::Event> event;
  event = m_interference.Add (duration, rxPowerDbm, sf, packet, frequencyMHz);

  // Cycle over the receive paths to check availability to receive the packet
  std::list<Ptr<SimpleGatewayLoraPhy::ReceptionPath>>::iterator it;
  for (it = m_receptionPaths.begin (); it != m_receptionPaths.end (); ++it)
    {
      Ptr<SimpleGatewayLoraPhy::ReceptionPath> currentPath = *it;

      // If the receive path is available and listening on the channel of
      // interest, we have a candidate
      //listening on the channel of interst ?? comment ca alors !! il a rien qui carcterise le canal en question 
      if (currentPath->IsAvailable ())
        {
          // See whether the reception power is above or below the sensitivity
          // for that spreading factor
          double sensitivity = SimpleGatewayLoraPhy::sensitivity[unsigned (sf) - 7];

          if (rxPowerDbm < sensitivity) // Packet arrived below sensitivity
            {
              NS_LOG_INFO ("Dropping packet reception of packet with sf = "
                           << unsigned (sf) << " because under the sensitivity of " << sensitivity
                           << " dBm");

              if (m_device)
                {
                  m_underSensitivity (packet, m_device->GetNode ()->GetId ());
                }
              else
                {
                  m_underSensitivity (packet, 0);
                }

              // Since the packet is below sensitivity, it makes no sense to
              // search for another ReceivePath
              return;
            }
          else // We have sufficient sensitivity to start receiving
            {
              NS_LOG_INFO ("Scheduling reception of a packet, "
                           << "occupying one demodulator");

              // Block this resource
              currentPath->LockOnEvent (event);
              m_occupiedReceptionPaths++;

              // Schedule the end of the reception of the packet
              EventId endReceiveEventId =
                  Simulator::Schedule (duration, &LoraPhy::EndReceive, this, packet, event);

              currentPath->SetEndReceive (endReceiveEventId);

              // Make sure we don't go on searching for other ReceivePaths
              return;
            }
        } 

//==============================================================================================================================
// the currentPath is not available: so here we will test if the power of the  incomming packet is higher then the 
// packet that it is locked on!
     else if ((captureState ==SimpleGatewayLoraPhy::SWITCH) || (captureState ==SimpleGatewayLoraPhy::MIM))
        {
         // NS_LOG_DEBUG("ELSE");
          //1-Get the packet that  the current path loked on ! ( since the currentpath is not available: it means that 
          // it is locked to one other packet)
          // event= the new arriving event
          // event1= the Receiver Locked On event
          //2-Collect informations about both events
          Ptr<LoraInterferenceHelper::Event> event1;
          event1= currentPath->GetEvent();
          Ptr<Packet> packet1;
          packet1 = event1->GetPacket();
          //get the frequency and the Spreading Factor of the event
          double event1Frequency;
          uint8_t event1SF;
          double eventFrequency;
          uint8_t eventSF;
          event1Frequency = event1->GetFrequency();
          event1SF = event1->GetSpreadingFactor();
          eventFrequency=event->GetFrequency();
          eventSF = event->GetSpreadingFactor();
          if ((event1Frequency==eventFrequency) && (event1SF==eventSF))
//........................................................................................................................
           {
            Time event1StartTime = event1->GetStartTime ();
            //NS_LOG_DEBUG("event1StartTime"<<event1StartTime);
            Time event1PreambleDuration = event1->GetPreambleDuration();
            //NS_LOG_DEBUG("event1PreambleDuration"<<event1PreambleDuration);
            Time shiftPreamble = event1StartTime + event1PreambleDuration;
            //NS_LOG_DEBUG("shiftPreamble"<<shiftPreamble);
            Time event1HeaderDuration =event1->GetHeaderDuration(); 
            // Time event1HeaderDuration = Time(300*1000000);
            // NS_LOG_UNCOND("e=EVENT HEADER DURATION"<<event1HeaderDuration);
            Time shiftHeader = shiftPreamble + event1HeaderDuration;
            // NS_LOG_DEBUG("shiftHeader"<<shiftHeader);
            //3-Get the informations related to the incoming packet which is event
            Time eventStartTime = event->GetStartTime ();
            // NS_LOG_DEBUG("eventStartTime"<<eventStartTime);
            //4-Get the sinr
            double sinr = m_interference.GetSINR(event,event1);
            // NS_LOG_DEBUG("SINR"<< sinr);

            //---------------------------------------------------------------------------------
            if ( // START 
            //5-when sirn >=6dB and this packet arrives after preamble and during Header then lock on the new!
            //--------------------------------------------------------------SWICTH  -------------------------------------------------------------------

            ( ((sinr >= 6) && ((eventStartTime>shiftPreamble) && (eventStartTime<shiftHeader))) && (captureState ==SimpleGatewayLoraPhy::SWITCH))
            //--------------------------------------------------------------or-------------------------------------------------------------------
                                                                          ||
                                                                          
            //------------------------------------------------------------MIM--------------------------------------------------------------------  
            //6- (when sirn >=6dB and this packet arrives after preamble and during Header ) OR
            //  (when sirn >=10dB and this packet arrives after  Header )  then lock on the new 
            ((((sinr >= 6)&& ((eventStartTime>shiftPreamble) && (eventStartTime<shiftHeader))) || ((sinr >= 8) && (eventStartTime>shiftPreamble))) && (captureState ==SimpleGatewayLoraPhy::MIM))
               ) //END
            //  if (((sinr >= 6)&& ((eventStartTime>shiftPreamble) && (eventStartTime<shiftHeader))) || ((sinr >= 6) && (eventStartTime>shiftPreamble)))
            //---------------------------------------------------------------------------------
          
            {

              // NS_LOG_DEBUG("OK");
              // NS_LOG_DEBUG("FIND A PACKET THAT ARRIVED AFTER PREAMBLE AND DURING HEADERTIME");
              //a- fire a trace source or somthing to notify that the previous  packet was dropped
              // ou bien tager le paquet 
              // Call the callback for reception interrupted by transmission
              // Fire the trace source
              //since I lost the trace source becaus eof misktake in git , I will reimplement it after 
              // I will call the same callcback resposible for lost becaus eof interference!
              // b- Cancel the scheduled EndReceive call for the actual loked on packet
              // currentPath->GetEndReceive () : we get the endReceiveEventId of the previously scheduled EndReceive() 
              // for the acual packet that is dropped
                if (m_device)
                {
                    m_interferedPacket (packet1, m_device->GetNode ()->GetId ());
                }
                else
                {
                     m_interferedPacket (packet1, 0);
                }
              Simulator::Cancel (currentPath->GetEndReceive ());

               //c- lock on the new packet
               currentPath->SetEvent (event);

               //d- Schedule the end of the reception of the packet
               EventId endReceiveEventId =
                  Simulator::Schedule (duration, &LoraPhy::EndReceive, this, packet, event);
              currentPath->SetEndReceive (endReceiveEventId);
           return;
              
           } 
//........................................................................................................................           
        }
       
        } 

//==============================================================================================================================

  }
 
  // If we get to this point, there are no demodulators we can use
  NS_LOG_INFO ("Dropping packet reception of packet with sf = "
               << unsigned (sf) << " and frequency " << frequencyMHz
               << "MHz because no suitable demodulator was found");

  // Fire the trace source
  if (m_device)
    {
      m_noMoreDemodulators (packet, m_device->GetNode ()->GetId ());
    }
  else
    {
      m_noMoreDemodulators (packet, 0);
    }
}

void
SimpleGatewayLoraPhy::EndReceive (Ptr<Packet> packet, Ptr<LoraInterferenceHelper::Event> event)
{
  NS_LOG_FUNCTION (this << packet << *event);

  // Call the trace source
  m_phyRxEndTrace (packet);

  // Call the LoraInterferenceHelper to determine whether there was
  // destructive interference. If the packet is correctly received, this
  // method returns a 0.
  uint8_t packetDestroyed = 0;
  packetDestroyed = m_interference.IsDestroyedByInterference (event);
  uint64_t eventId= event->GetPacket()->GetUid();
   // Call the LoraInterferenceHelper to determine whether the packet 
   //is destroyed by collision, If the packet survived collision , this
  // method returns a 0.
  uint8_t packetSurvivesCollision=0;
  uint8_t packetSurvivesCollision2=0;
  //NS_LOG_DEBUG(captureState);
  switch (captureState)
  {
    case SimpleGatewayLoraPhy::MAX:
    packetSurvivesCollision=m_interference.IsDestroyedByCollisions(event);
    break;
    case SimpleGatewayLoraPhy::SUMPOW:
    packetSurvivesCollision=m_interference.IsDestroyedByCollisionsPowSum(event);
    packetSurvivesCollision2=m_interference.IsDestroyedByCollisions2(event);
    break;
    default:
    packetSurvivesCollision=m_interference.IsDestroyedByCollisions2(event);
  }
 /* 
if ((packetSurvivesCollision== uint8_t(0))&&(packetSurvivesCollision2== uint8_t(0)))
    {
      /// double rssi=event->GetRxPowerdBm();
      //NS_LOG_DEBUG("-------packet SURVIVED collision  from SUMPOWER with RSSI: -----------" << rssi << "PoI id is : "<< eventId );
      //NS_LOG_DEBUG("packet did not survived collision from COLLISION2 ");
      //if (packetSurvivesCollision2!= uint8_t(0))
     // {
       
         // NS_LOG_DEBUG("-------packet did not survived collision from COLLISION2 but from SUMPOWER with RSSI: -----------" << rssi << "PoI id is : "<< eventId );
      //}
     NS_LOG_DEBUG (" we are in (0,0) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
  else if ((packetSurvivesCollision== uint8_t(0))&&(packetSurvivesCollision2!= uint8_t(0)))
    {
      NS_LOG_DEBUG (" we are in (0,1) state" << "testcol" << "PoI id is : " << eventId );
    }
  else if ((packetSurvivesCollision!= uint8_t(0))&&(packetSurvivesCollision2== uint8_t(0)))
    {
 NS_LOG_DEBUG (" we are in (1,0) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
  else 
    {
      NS_LOG_DEBUG (" we are in (0,0) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
*/

  //check if packets survives collision
  if (packetSurvivesCollision!= uint8_t(0))
    {
      NS_LOG_DEBUG (" we are in (1,.) state"<<"testcol"<<"PoI id is : "<< eventId );
      NS_LOG_DEBUG("packet did not survived collision");

    if (packetSurvivesCollision2!= uint8_t(0))
     {
      NS_LOG_DEBUG("packet did not survived collision");
       NS_LOG_DEBUG (" we are in (1,1) state"<<"testcol"<<"PoI id is : "<< eventId );
     }
    else 
     {
      NS_LOG_DEBUG ("Packet with SF " << unsigned (event->GetSpreadingFactor ())
                                    << " survived collision");
      NS_LOG_DEBUG (" we are in (1,0) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
    }
  else 
    {
      NS_LOG_DEBUG (" we are in (0,.) state"<<"testcol"<<"PoI id is : "<< eventId );
      NS_LOG_DEBUG ("Packet with SF " << unsigned (event->GetSpreadingFactor ())
                                    << " survived collision");
    if (packetSurvivesCollision2!= uint8_t(0))
     {
      NS_LOG_DEBUG("packet did not survived collision");
       NS_LOG_DEBUG (" we are in (0,1) state"<<"testcol"<<"PoI id is : "<< eventId );
     }
    else 
    {
      NS_LOG_DEBUG ("Packet with SF " << unsigned (event->GetSpreadingFactor ())
                                    << " survived collision");
      NS_LOG_DEBUG (" we are in (0,0) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
    }

/*
  if (packetSurvivesCollision2!= uint8_t(0))
    {
      NS_LOG_DEBUG("packet did not survived collision");
       NS_LOG_DEBUG (" we are in (.,1) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
  else 
    {
      NS_LOG_DEBUG ("Packet with SF " << unsigned (event->GetSpreadingFactor ())
                                    << " survived collision");
      NS_LOG_DEBUG (" we are in (.,0) state"<<"testcol"<<"PoI id is : "<< eventId );
    }
*/
  // Check whether the packet was destroyed

// 0: GOURSAUD; 1: ALOHA
  bool collisionMatrix=0;
  collisionMatrix =m_interference.collisionMatrix;
//  NS_LOG_UNCOND(collisionMatrix);
/* collisionMatrix is an enum =1  if aloha else 0 (goursaud ) 

 ------------aloha---------------------------------------------------
if ((packetDestroyed != uint8_t (0)) &&  (collisionMatrix) )   // if collisionMatrix=1 ==> aloha 

------------goursaud-------------------------------------------------------------
if ( ( (packetDestroyed != uint8_t (0)) || (packetSurvivesCollision!= uint8_t(0)) ) 
                                         && (!collisionMatrix) )  // if collisionMatrix=0 ==> goursaud 

------------------------------short explanation-----------------------------------
in other words I wanted to diffrentiate between aloha state and goursaud in the same line : if aloha then
only test packet interference throught packetdestroyed value otherwise  if goursaud then
 test inteference and  capture through packetSurvivesCollision 
*/
//if (packetDestroyed != uint8_t (0)) // in case I want to get back to the cumulative energy implemn by default
// decomnet the line above and  comment the new if 

//NS_LOG_UNCOND(int(packetDestroyed));
if (
 // (  (packetSurvivesCollision!= uint8_t(0))  && (!collisionMatrix) ) 
  ( ( (packetDestroyed != uint8_t (0)) || (packetSurvivesCollision!= uint8_t(0)) ) && (!collisionMatrix) ) 
                                       ||                           
  ( (packetDestroyed != uint8_t (0)) &&  (collisionMatrix) )                                            
   )  
    {
      NS_LOG_DEBUG ("packetDestroyed by " << unsigned (packetDestroyed));

      // Update the packet's LoraTag
      LoraTag tag;
      packet->RemovePacketTag (tag);
      tag.SetDestroyedBy (packetDestroyed);
      packet->AddPacketTag (tag);

      // Fire the trace source
      if (m_device)
        {
          m_interferedPacket (packet, m_device->GetNode ()->GetId ());
        }
      else
        {
          m_interferedPacket (packet, 0);
        }
    }
  else // Reception was correct
    {
      NS_LOG_DEBUG("packet received correctly " << unsigned (packet->GetUid())) ;
      NS_LOG_INFO ("Packet with SF " << unsigned (event->GetSpreadingFactor ())
                                     << " received correctly");

      // Fire the trace source
      if (m_device)
        {
          m_successfullyReceivedPacket (packet, m_device->GetNode ()->GetId ());
        }
      else
        {
          m_successfullyReceivedPacket (packet, 0);
        }

      // Forward the packet to the upper layer
      if (!m_rxOkCallback.IsNull ())
        {
          // Make a copy of the packet
          // Ptr<Packet> packetCopy = packet->Copy ();

          // Set the receive power and frequency of this packet in the LoraTag: this
          // information can be useful for upper layers trying to control link
          // quality.
          LoraTag tag;
          packet->RemovePacketTag (tag);
          tag.SetReceivePower (event->GetRxPowerdBm ());
          tag.SetFrequency (event->GetFrequency ());
          packet->AddPacketTag (tag);

          m_rxOkCallback (packet);
        }
    }

  // Search for the demodulator that was locked on this event to free it.

  std::list<Ptr<SimpleGatewayLoraPhy::ReceptionPath>>::iterator it;

  for (it = m_receptionPaths.begin (); it != m_receptionPaths.end (); ++it)
    {
      Ptr<SimpleGatewayLoraPhy::ReceptionPath> currentPath = *it;

      if (currentPath->GetEvent () == event)
        {
          currentPath->Free ();
          m_occupiedReceptionPaths--;
          return;
        }
    }
}

} // namespace lorawan
} // namespace ns3
