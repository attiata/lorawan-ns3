/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2017 University of Padova
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Davide Magrin <magrinda@dei.unipd.it>
 */

#include "ns3/lora-interference-helper.h"
#include "ns3/log.h"
#include "ns3/enum.h"
#include <limits>

namespace ns3 {
namespace lorawan {

NS_LOG_COMPONENT_DEFINE ("LoraInterferenceHelper");

/***************************************
 *    LoraInterferenceHelper::Event    *
 ***************************************/

// Event Constructor
LoraInterferenceHelper::Event::Event (Time duration, double rxPowerdBm, uint8_t spreadingFactor,
                                      Ptr<Packet> packet, double frequencyMHz)
    : m_startTime (Simulator::Now ()),
      m_endTime (m_startTime + duration),
      m_sf (spreadingFactor),
      m_rxPowerdBm (rxPowerdBm),
      m_packet (packet),
      m_frequencyMHz (frequencyMHz)
{
  // NS_LOG_FUNCTION_NOARGS ();
}

// Event Destructor
LoraInterferenceHelper::Event::~Event ()
{
  // NS_LOG_FUNCTION_NOARGS ();
}

// Getters
Time
LoraInterferenceHelper::Event::GetStartTime (void) const
{
  return m_startTime;
}

Time
LoraInterferenceHelper::Event::GetEndTime (void) const
{
  return m_endTime;
}

Time
LoraInterferenceHelper::Event::GetPreambleDuration (void) const
{
  uint32_t nPreamble = 8; // number of  preamble symbols : it is fixed 8 in loRaWanMac::SetNPreambleSymbole
  double bandwidthHz = 125000; //!< Bandwidth in Hz
  double tSym = pow (2, int (m_sf)) / (bandwidthHz);
  Time tPreamble = Seconds ((double (nPreamble) + 4.25) * tSym); // en seconds!
  return tPreamble;
}
Time
LoraInterferenceHelper::Event::GetHeaderDuration (void) const
{
  /*
  duration of the header is equal to :
  HeaderDuration = CEIL((28+16)*(CR+1)*1/(4*(SF-2DE)))
  where 
  CR: coding rate from 1 to 4(1--> 5/4; 2-->6/4; 3-->7/4; 4-->8/4)
  SF: spreading factor
  DE: low data rate optimization; 1 for SF=12 or SF=11, and 0 otherwise 
  */
  double bandwidthHz = 125000; //!< Bandwidth in Hz
  double tSym = pow (2, int (m_sf)) / (bandwidthHz);
  Time headerDuration;
  if (m_sf>=11)
  {

   headerDuration = Seconds(tSym*(std::ceil((55/(m_sf-2)))));

  }
  else
  {
   headerDuration = Seconds(tSym*(std::ceil((55/(m_sf)))));
  }
   return   headerDuration;
}
Time
LoraInterferenceHelper::Event::GetDuration (void) const
{
  return m_endTime - m_startTime;
}
double
LoraInterferenceHelper::Event::GetRxPowerdBm (void) const
{
  return m_rxPowerdBm;
}

uint8_t
LoraInterferenceHelper::Event::GetSpreadingFactor (void) const
{
  return m_sf;
}

Ptr<Packet>
LoraInterferenceHelper::Event::GetPacket (void) const
{
  return m_packet;
}

double
LoraInterferenceHelper::Event::GetFrequency (void) const
{
  return m_frequencyMHz;
}

void
LoraInterferenceHelper::Event::Print (std::ostream &stream) const
{
  stream << "(" << m_startTime.GetSeconds () << " s - " << m_endTime.GetSeconds () << " s), SF"
         << unsigned (m_sf) << ", " << m_rxPowerdBm << " dBm, " << m_frequencyMHz << " MHz";
}

std::ostream &
operator<< (std::ostream &os, const LoraInterferenceHelper::Event &event)
{
  event.Print (os);

  return os;
}

/****************************
 *  LoraInterferenceHelper  *
 ****************************/
// This collision matrix can be used for comparisons with the performance of Aloha
// systems, where collisions imply the loss of both packets.
double inf = std::numeric_limits<double>::max ();
double minf = std::numeric_limits<double>::min ();
std::vector<std::vector<double>> LoraInterferenceHelper::collisionSnirAloha = {
    //   7   8   9  10  11  12
    {inf, minf, minf, minf, minf, minf}, // SF7
    {minf, inf, minf, minf, minf, minf}, // SF8
    {minf, minf, inf, minf, minf, minf}, // SF9
    {minf, minf, minf, inf, minf, minf}, // SF10
    {minf, minf, minf, minf, inf, minf}, // SF11
    {minf, minf, minf, minf, minf, inf} // SF12
};

// LoRa Collision Matrix (Goursaud)
// Values are inverted w.r.t. the paper since here we interpret this as an
// _isolation_ matrix instead of a cochannel _rejection_ matrix like in
// Goursaud's paper.
std::vector<std::vector<double>> LoraInterferenceHelper::collisionSnirGoursaud = {
    // SF7  SF8  SF9  SF10 SF11 SF12
    {6, -16, -18, -19, -19, -20}, // SF7
    {-24, 6, -20, -22, -22, -22}, // SF8
    {-27, -27, 6, -23, -25, -25}, // SF9
    {-30, -30, -30, 6, -26, -28}, // SF10
    {-33, -33, -33, -33, 6, -29}, // SF11
    {-36, -36, -36, -36, -36, 6} // SF12
};

LoraInterferenceHelper::CollisionMatrix LoraInterferenceHelper::collisionMatrix =
    LoraInterferenceHelper::GOURSAUD;

NS_OBJECT_ENSURE_REGISTERED (LoraInterferenceHelper);

void
LoraInterferenceHelper::SetCollisionMatrix (
    enum LoraInterferenceHelper::CollisionMatrix collisionMatrix)
{
  switch (collisionMatrix)
    {
    case LoraInterferenceHelper::ALOHA:
      NS_LOG_DEBUG ("Setting the ALOHA collision matrix");
      m_collisionSnir = LoraInterferenceHelper::collisionSnirAloha;
      break;
    case LoraInterferenceHelper::GOURSAUD:
      NS_LOG_DEBUG ("Setting the GOURSAUD collision matrix");
      m_collisionSnir = LoraInterferenceHelper::collisionSnirGoursaud;
      break;
    }
}

TypeId
LoraInterferenceHelper::GetTypeId (void)
{
  static TypeId tid =
      TypeId ("ns3::LoraInterferenceHelper").SetParent<Object> ().SetGroupName ("lorawan");

  return tid;
}

LoraInterferenceHelper::LoraInterferenceHelper ()
    : m_collisionSnir (LoraInterferenceHelper::collisionSnirGoursaud)
{
  NS_LOG_FUNCTION (this);

  SetCollisionMatrix (collisionMatrix);
}

LoraInterferenceHelper::~LoraInterferenceHelper ()
{
  NS_LOG_FUNCTION (this);
}

Time LoraInterferenceHelper::oldEventThreshold = Seconds (2);

Ptr<LoraInterferenceHelper::Event>
LoraInterferenceHelper::Add (Time duration, double rxPower, uint8_t spreadingFactor,
                             Ptr<Packet> packet, double frequencyMHz)
{

  NS_LOG_FUNCTION (this << duration.GetSeconds () << rxPower << unsigned (spreadingFactor) << packet
                        << frequencyMHz);

  // Create an event based on the parameters
  Ptr<LoraInterferenceHelper::Event> event = Create<LoraInterferenceHelper::Event> (
      duration, rxPower, spreadingFactor, packet, frequencyMHz);

  // Add the event to the list
  m_events.push_back (event);

  // Clean the event list
  if (m_events.size () > 100)
    {
      CleanOldEvents ();
    }

  return event;
}

void
LoraInterferenceHelper::CleanOldEvents (void)
{
  NS_LOG_FUNCTION (this);

  // Cycle the events, and clean up if an event is old.
  for (auto it = m_events.begin (); it != m_events.end ();)
    {
      if ((*it)->GetEndTime () + oldEventThreshold < Simulator::Now ())
        {
          it = m_events.erase (it);
        }
      it++;
    }
}

std::list<Ptr<LoraInterferenceHelper::Event>>
LoraInterferenceHelper::GetInterferers ()
{
  return m_events;
}

void
LoraInterferenceHelper::PrintEvents (std::ostream &stream)
{
  NS_LOG_FUNCTION_NOARGS ();

  stream << "Currently registered events:" << std::endl;

  for (auto it = m_events.begin (); it != m_events.end (); it++)
    {
      (*it)->Print (stream);
      stream << std::endl;
    }
}

uint8_t
LoraInterferenceHelper::IsDestroyedByInterference (Ptr<LoraInterferenceHelper::Event> event)
{
  NS_LOG_FUNCTION (this << event);

  NS_LOG_INFO ("Current number of events in LoraInterferenceHelper: " << m_events.size ());

  // We want to see the interference affecting this event: cycle through events
  // that overlap with this one and see whether it survives the interference or
  // not.

  // Gather information about the event
  double rxPowerDbm = event->GetRxPowerdBm ();
  uint8_t sf = event->GetSpreadingFactor ();
  double frequency = event->GetFrequency ();

  // Handy information about the time frame when the packet was received
  Time now = Simulator::Now ();
  Time duration = event->GetDuration ();
  Time packetStartTime = now - duration;
  Time packetEndTime = now;

  // Get the list of interfering events
  std::list<Ptr<LoraInterferenceHelper::Event>>::iterator it;

  // Energy for interferers of various SFs
  std::vector<double> cumulativeInterferenceEnergy (6, 0);

  // Cycle over the events
  for (it = m_events.begin (); it != m_events.end ();)
    {
      // Pointer to the current interferer
      Ptr<LoraInterferenceHelper::Event> interferer = *it;

      // Only consider the current event if the channel is the same: we
      // assume there's no interchannel interference. Also skip the current
      // event if it's the same that we want to analyze.
      //I exclude same SF because we treat this as a collision in the function IsDestroyedByCollision
 // NS_LOG_UNCOND(collisionMatrix);
  switch (collisionMatrix)
    {
    case LoraInterferenceHelper::GOURSAUD:
       if (!(interferer->GetFrequency () == frequency) || interferer == event || (interferer->GetSpreadingFactor() == sf))
     //if (!(interferer->GetFrequency () == frequency) || interferer == event)
        {
          NS_LOG_DEBUG ("Different channel or same event or same SF");
          it++;
       //   NS_LOG_UNCOND ("GOURSAUD");
          continue; // Continues from the first line inside the for cycle
        }
      
    case LoraInterferenceHelper::ALOHA:
       //if (!(interferer->GetFrequency () == frequency) || interferer == event || (interferer->GetSpreadingFactor() == sf))
      if (!(interferer->GetFrequency () == frequency) || interferer == event)
        {
          NS_LOG_DEBUG ("Different channel or same event or same SF");
          it++;
        //  NS_LOG_UNCOND ("ALOHA");
          continue; // Continues from the first line inside the for cycle
        }


    }

/*
     if (!(interferer->GetFrequency () == frequency) || (interferer == event) || (interferer->GetSpreadingFactor() == sf))
     //if (!(interferer->GetFrequency () == frequency) || interferer == event)
        {
          NS_LOG_DEBUG ("Different channel or same event or same SF");
          it++;
          continue; // Continues from the first line inside the for cycle
        }
*/
      NS_LOG_DEBUG ("Interferer on same channel");

      // Gather information about this interferer
      uint8_t interfererSf = interferer->GetSpreadingFactor ();
      double interfererPower = interferer->GetRxPowerdBm ();
      Time interfererStartTime = interferer->GetStartTime ();
      Time interfererEndTime = interferer->GetEndTime ();

      NS_LOG_INFO ("Found an interferer: sf = " << unsigned (interfererSf)
                                                << ", power = " << interfererPower
                                                << ", start time = " << interfererStartTime
                                                << ", end time = " << interfererEndTime);

      // Compute the fraction of time the two events are overlapping
      Time overlap = GetOverlapTime (event, interferer);

      NS_LOG_DEBUG ("The two events overlap for " << overlap.GetSeconds () << " s.");

      // Compute the equivalent energy of the interference
      // Power [mW] = 10^(Power[dBm]/10)
      // Power [W] = Power [mW] / 1000
      double interfererPowerW = pow (10, interfererPower / 10) / 1000;
      // Energy [J] = Time [s] * Power [W]
      double interferenceEnergy = overlap.GetSeconds () * interfererPowerW;
      cumulativeInterferenceEnergy.at (unsigned (interfererSf) - 7) += interferenceEnergy;
      NS_LOG_DEBUG ("Interferer power in W: " << interfererPowerW);
      NS_LOG_DEBUG ("Interference energy: " << interferenceEnergy);
      it++;
    }

  // For each SF, check if there was destructive interference
  for (uint8_t currentSf = uint8_t (7); currentSf <= uint8_t (12); currentSf++)
    {
      NS_LOG_DEBUG ("Cumulative Interference Energy: "
                    << cumulativeInterferenceEnergy.at (unsigned (currentSf) - 7));

      // Use the computed cumulativeInterferenceEnergy to determine whether the
      // interference with this SF destroys the packet
      double signalPowerW = pow (10, rxPowerDbm / 10) / 1000;
      double signalEnergy = duration.GetSeconds () * signalPowerW;
      NS_LOG_DEBUG ("Signal power in W: " << signalPowerW);
      NS_LOG_DEBUG ("Signal energy: " << signalEnergy);
      // Check whether the packet survives the interference of this SF
      double snirIsolation = m_collisionSnir[unsigned (sf) - 7][unsigned (currentSf) - 7];
      NS_LOG_DEBUG ("The needed isolation to survive is " << snirIsolation << " dB");
      double snir =
          10 * log10 (signalEnergy / cumulativeInterferenceEnergy.at (unsigned (currentSf) - 7));
      NS_LOG_DEBUG ("The current SNIR is " << snir << " dB");

      if (snir >= snirIsolation)
        {
          // Move on and check the rest of the interferers
          NS_LOG_DEBUG ("Packet survived interference with SF " << currentSf);
        }
      else
        {
          NS_LOG_DEBUG ("Packet destroyed by interference with SF" << unsigned (currentSf));

          return currentSf;
        }
    }
  // If we get to here, it means that the packet survived all interference
  NS_LOG_DEBUG ("Packet survived all interference");

  // Since the packet was not destroyed, we return 0.
  return uint8_t (0);
}

void
LoraInterferenceHelper::ClearAllEvents (void)
{
  NS_LOG_FUNCTION_NOARGS ();

  m_events.clear ();
}
double
LoraInterferenceHelper::GetSINR (Ptr<LoraInterferenceHelper::Event> event1,
                                              Ptr<LoraInterferenceHelper::Event> event2)
{
  double event1_rssi_w=pow (10, event1->GetRxPowerdBm() / 10) / 1000;
  double event2_rssi_w=pow (10, event2->GetRxPowerdBm() / 10) / 1000;
  return 10*log10(event1_rssi_w/event2_rssi_w);
}
Time
LoraInterferenceHelper::GetOverlapTime (Ptr<LoraInterferenceHelper::Event> event1,
                                        Ptr<LoraInterferenceHelper::Event> event2)
{
  NS_LOG_FUNCTION_NOARGS ();

  // Create the value we will return later
  Time overlap;

  // Get handy values
  Time s1 = event1->GetStartTime (); // Start times
  Time s2 = event2->GetStartTime ();
  Time e1 = event1->GetEndTime (); // End times
  Time e2 = event2->GetEndTime ();

  // Non-overlapping events
  if (e1 <= s2 || e2 <= s1)
    {
      overlap = Seconds (0);
    }
  // event1 before event2
  else if (s1 < s2)
    {
      if (e2 < e1)
        {
          overlap = e2 - s2;
        }
      else
        {
          overlap = e1 - s2;
        }
    }
  // event2 before event1 or they start at the same time (s1 = s2)
  else
    {
      if (e1 < e2)
        {
          overlap = e1 - s1;
        }
      else
        {
          overlap = e2 - s1;
        }
    }

  return overlap;
}

uint8_t
LoraInterferenceHelper::IsDestroyedByCollisions (Ptr<LoraInterferenceHelper::Event> event)
{
  NS_LOG_FUNCTION (this << event);

  // Gather information about the event
  double rxPowerDbm = event->GetRxPowerdBm ();
  uint8_t sf = event->GetSpreadingFactor ();
  double frequency = event->GetFrequency ();

  // Get the list of interfering events
  std::list<Ptr<LoraInterferenceHelper::Event>>::iterator it;

  // the maximum interferer power of packets received with same SF as the event
  double maxPowerdbm = -200;

  // Cycle over the events
  for (it = m_events.begin (); it != m_events.end ();)
    {
      // Pointer to the current interferer
      Ptr<LoraInterferenceHelper::Event> interferer = *it;

      // Only consider the current event if the channel  and the SF are  the same.
      // Also skip the current event if it's the same that we want to analyze.
      if (!(interferer->GetFrequency () == frequency) || interferer == event ||
          !(interferer->GetSpreadingFactor () == sf))
        {
          NS_LOG_DEBUG ("Different channel or same event or not same SF ");
          it++;
          continue; // Continues from the first line inside the for cycle
        }

      NS_LOG_DEBUG ("Interferer on same channel and same sf");

      // Gather information about this interferer
      double interfererPower = interferer->GetRxPowerdBm ();

      NS_LOG_DEBUG ("interferer signal power in dbm: " << interfererPower);

      //check if this interferer overlap with the event
      Time overlap = GetOverlapTime (event, interferer);

      NS_LOG_DEBUG ("The two events overlap for " << overlap.GetSeconds () << " s.");
      // if the interferer overlap with the event then
      //compare the received power to the maximum yet saved, if its higher then update the value
      // to the actual value!
      if (overlap > Time (0))
        {
          if (interfererPower > maxPowerdbm)
            {
              maxPowerdbm = interfererPower;
            }
        }

      it++;
    }
  NS_LOG_DEBUG ("max Signal power of the collided packet in dbm: " << maxPowerdbm);
  // test if the event survived the collision with the event with the maximum power
  // convert the receivedd power of the event to watt
  double signalPowerW = pow (10, rxPowerDbm / 10) / 1000;
  double maxPowerW = pow (10, maxPowerdbm / 10) / 1000;
  NS_LOG_DEBUG ("Signal power in W: " << signalPowerW);
  NS_LOG_DEBUG ("max power of collided packet " << maxPowerW);
  //get the sinr of this SF which is 6dB
  double snirIsolation = m_collisionSnir[unsigned (sf) - 7][unsigned (sf) - 7];
  NS_LOG_DEBUG ("snirIsolation = " << snirIsolation);
  //the ratio between two powers
  double ratio = 10 * log10 (signalPowerW / maxPowerW);
  // NS_LOG_DEBUG ("Ratio in dB between powers is " << ratio);
  if (ratio >= snirIsolation)
    {
      NS_LOG_DEBUG ("Packet survived collision with SF " << sf);
    }
  else
    {
      NS_LOG_DEBUG ("Packet destroyed collision with SF " << sf);
      return 1;
    }

  return uint8_t (0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

uint8_t
LoraInterferenceHelper::IsDestroyedByCollisions2 (Ptr<LoraInterferenceHelper::Event> event)
{
  NS_LOG_FUNCTION (this << event);

  // Gather information about the event
  double rxPowerDbm = event->GetRxPowerdBm ();
  uint8_t sf = event->GetSpreadingFactor ();
  double frequency = event->GetFrequency ();
  Time eventStartTime = event->GetStartTime ();
  Time eventEndTime = event->GetEndTime ();
  uint64_t eventId= event->GetPacket()->GetUid();
  // calculer le preamble duration
  uint32_t nPreamble = 8; // number of  preamble symbols : it is fixed 8 in loRaWanMac::SetNPreambleSymbole
  double bandwidthHz = 125000; //!< Bandwidth in Hz
  double tSym = pow (2, int (sf)) / (bandwidthHz);
  Time tPreamble = Seconds ((double (nPreamble) + 4.25) * tSym); // en seconds!!!
  // NS_LOG_DEBUG("tPreamble"<<tPreamble);
  Time shift = eventStartTime + tPreamble;
  /*NS_LOG_DEBUG ("event's info: " << ", start time = " << eventStartTime
                                                << ", start time+preamble = " << shift
                                                << ", end time = " << eventEndTime);*/

  // Get the list of interfering events
  std::list<Ptr<LoraInterferenceHelper::Event>>::iterator it;

  // the maximum interferer power of packets received with same SF as the event and arrives during the preamble period
  double maxPowerdbm1 = -400;
  // the maximum interferer power of packets received with same SF as the event and arrives after the preamble period
  double maxPowerdbm2 = -400;
  // convert the receivedd power of the event to watt
  double signalPowerW = pow (10, rxPowerDbm / 10) / 1000;
  // Cycle over the events
  for (it = m_events.begin (); it != m_events.end ();)
    {
      // Pointer to the current interferer
      Ptr<LoraInterferenceHelper::Event> interferer = *it;

      // Only consider the current event if the channel  and the SF are  the same.
      // Also skip the current event if it's the same that we want to analyze.
      if (!(interferer->GetFrequency () == frequency) || interferer == event ||
          !(interferer->GetSpreadingFactor () == sf))
        {
          NS_LOG_DEBUG ("Different channel or same event or not same SF ");
          it++;
          continue; // Continues from the first line inside the for cycle
        }

      NS_LOG_DEBUG ("Interferer on same channel and same sf");

      //check if this interferer overlap with the event
      Time overlap = GetOverlapTime (event, interferer);

      NS_LOG_DEBUG ("The two events overlap for " << overlap.GetSeconds () << " s.");
      // if the interferer overlap with the event then
      //1-compare the received power to the maximum yet saved, if its higher then update the value
      // to the actual value!
      if (overlap > Time (0))
        {
          NS_LOG_DEBUG ("Interferer that overalap with the packet");

          // Gather information about this interferer
          double interfererPower = interferer->GetRxPowerdBm ();
          Time interfererStartTime = interferer->GetStartTime ();

          if (interfererStartTime < shift)
            {
              if (interfererPower > maxPowerdbm1)
                {
                  maxPowerdbm1 = interfererPower;
                }
            }
          //2-compare if this event arrives after
          else // if ((interfererStartTime >= shift)  )
            {
              if (interfererPower > maxPowerdbm2)
                {
                  maxPowerdbm2 = interfererPower;
                }
            }
        }

      it++;
    }

  // convert the two max receivedd powers of the interferers of the two groups  to watt
  double maxPowerW1 = pow (10, maxPowerdbm1 / 10) / 1000;
  double maxPowerW2 = pow (10, maxPowerdbm2 / 10) / 1000;
  //get the sinr of this SF which is 6dB
  double snirIsolation = m_collisionSnir[unsigned (sf) - 7][unsigned (sf) - 7];
  NS_LOG_DEBUG ("snirIsolation = " << snirIsolation);
  //the ratio between two powers
  double sinr1 = 10 * log10 (signalPowerW / maxPowerW1);
  double sinr2 = 10 * log10 (signalPowerW / maxPowerW2);
  NS_LOG_DEBUG ("IsDestroyedByCollisions2:Ratio 1 in dB between powers is " << sinr1 << "PoI id is : "<< eventId);
  NS_LOG_DEBUG ("IsDestroyedByCollisions2:Ratio 2 in dB between powers is " << sinr2 << "PoI id is : "<< eventId);
  if (sinr1 >= 6)
    {
      if (sinr2 >= 0)
        {
           NS_LOG_DEBUG ("IsDestroyedByCollisions2:Packet survived collision with SF"  << "PoI id is : "<< eventId);
        }
      else
        {
          NS_LOG_DEBUG ("IsDestroyedByCollisions2:Packet destroyed collision with SF / sinr2"  << "PoI id is : "<< eventId);
          return 1;
        }
    }
  else
    {
      NS_LOG_DEBUG ("IsDestroyedByCollisions2:Packet destroyed collision with SF % sinr1"  << "PoI id is : "<< eventId);
      return 1;
    }

  return uint8_t (0);
}
uint8_t
LoraInterferenceHelper::IsDestroyedByCollisionsPowSum (Ptr<LoraInterferenceHelper::Event> event)
{
  NS_LOG_FUNCTION (this << event);
  // Gather information about the event
  double rxPowerDbm = event->GetRxPowerdBm ();
  uint8_t sf = event->GetSpreadingFactor ();
  double frequency = event->GetFrequency ();
  Time eventStartTime=event->GetStartTime();
   //uint64_t eventId= event->GetPacket()->GetUid();

  // Get the list of interfering events
  std::list<Ptr<LoraInterferenceHelper::Event>>::iterator it;

  // Initilaizing the sum 
  // in this scenario as the previous one , we will consider two groups of interferring packets
  /*
  Groupe1: interfreres that arrives before the packet start time 
  Groupe 2: interferers that arrives after the packet start time
  */
  double sumPowW1=0;
  double sumPowW2=0;


  // Cycle over the events
  for (it = m_events.begin (); it != m_events.end ();)
    {
      // Pointer to the current interferer
      Ptr<LoraInterferenceHelper::Event> interferer = *it;

      // Only consider the current event if the channel  and the SF are  the same.
      // Also skip the current event if it's the same that we want to analyze.
      if (!(interferer->GetFrequency () == frequency) || interferer == event ||
          !(interferer->GetSpreadingFactor () == sf))
        {
          NS_LOG_DEBUG ("Different channel or same event or not same SF ");
          it++;
          continue; // Continues from the first line inside the for cycle
        }

      NS_LOG_DEBUG ("Interferer on same channel and same sf");



     // Compute the fraction of time the two events are overlapping
      Time overlap = GetOverlapTime (event, interferer);

      NS_LOG_DEBUG ("The two events overlap for " << overlap.GetSeconds () << " s.");
      // if the interferer overlap with the event then
      //compare the received power to the maximum yet saved, if its higher then update the value
      // to the actual value!
      if (overlap > Time (0))
        {

          NS_LOG_DEBUG ("Interferer that overalap with the packet");
          // Gather information about this interferer
           double interfererPower = interferer->GetRxPowerdBm ();
           Time interfererStartTime=interferer->GetStartTime ();

      NS_LOG_DEBUG ("interferer signal power in dbm: " << interfererPower);
          if (interfererStartTime > eventStartTime)
            {
              double interfererPowerw= pow (10, interfererPower / 10) / 1000;
              sumPowW1 += interfererPowerw;
            }
            else 
            {
              double interfererPowerw= pow (10, interfererPower / 10) / 1000;
              sumPowW2 += interfererPowerw;
            }
        }

      it++;
    }
  // test if the event survived the collision with the event with the oth s
  // convert the receivedd power of the event to watt
  double signalPowerW = pow (10, rxPowerDbm / 10) / 1000;

  NS_LOG_DEBUG ("Signal power in W: " << signalPowerW);
  NS_LOG_DEBUG ("max power of collided packet " << sumPowW1);
 // NS_LOG_UNCOND ("max power of collided packet " << sumPowW);
   
  //the ratio between two powers
  // here im testing and evaluationg the cases that cause a packet drop otherwise the packet is saved 
  // the first case where the first sumpower is different then 0 and the ration is lower then 6.
  if (!(sumPowW2==0))
     { //compute the ratio 
       // double ratio2 = 10 * log10 (signalPowerW / sumPowW2);
        //if (ratio2 <= 6)
       // {
          //droped packet 
          return 1;
       // }
     }
  // if we are here that means either the sumpower2 is 0 or its higher then 6, then we need to test the remaing sumpower of 
  // packets arriving after the packet of interest 
  // here we test id the  sumpower1 is different then 0 and the ration is lower then 0, the packet is dropped .
  if (!(sumPowW1==0))
     { //compute the ratio 
        double ratio1 = 10 * log10 (signalPowerW / sumPowW1);
        if (ratio1 <= 0)
        {
          //dropped packet
          return 1;
        }
        //unessecary to implmenet the else( where the pacekt is saved ) because if we arrrive at this point the function
        // will return 0 (packet sutviced)
     }
  return uint8_t (0);
}

} // namespace lorawan
} // namespace ns3
/*
  ----------------------------------------------------------------------------

  // Event1 starts before Event2
  if (s1 < s2)
  {
  // Non-overlapping events
  if (e1 < s2)
  {
  overlap = Seconds (0);
  }
  // event1 contains event2
  else if (e1 >= e2)
  {
  overlap = e2 - s2;
  }
  // Partially overlapping events
  else
  {
  overlap = e1 - s2;
  }
  }
  // Event2 starts before Event1
  else
  {
  // Non-overlapping events
  if (e2 < s1)
  {
  overlap = Seconds (0);
  }
  // event2 contains event1
  else if (e2 >= e1)
  {
  overlap = e1 - s1;
  }
  // Partially overlapping events
  else
  {
  overlap = e2 - s1;
  }
  }
  return overlap;
  }
  }
  }
*/
