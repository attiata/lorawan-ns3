This work represents the extention and the modification of the LoRaWAN module detailed here https://apps.nsnam.org/app/lorawan/.

This extended module includes the implementation of the five receive schemes; detailed in the paper "Message in Message for improved LoRaWAN capacity" as follows

1. Aloha: collision
2. Simple capture 
3. Advanced capture 
4. Physical capture
5. Message in Message
