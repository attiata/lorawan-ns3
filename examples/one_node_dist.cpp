/*
 * This script simulates a  scenario with multiple gateways and end
 * devices. The metric of interest for this script is the throughput of the
 * network.
 */

#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/end-device-lorawan-mac.h"
#include "ns3/gateway-lorawan-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/pointer.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/node-container.h"
#include "ns3/mobility-helper.h"
#include "ns3/position-allocator.h"
#include "ns3/double.h"
#include "ns3/random-variable-stream.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/network-server-helper.h"
#include "ns3/correlated-shadowing-propagation-loss-model.h"
#include "ns3/building-penetration-loss.h"
#include "ns3/building-allocator.h"
#include "ns3/buildings-helper.h"
#include "ns3/forwarder-helper.h"
#include <algorithm>
#include <ctime>

using namespace ns3;
using namespace lorawan;

NS_LOG_COMPONENT_DEFINE ("ONENODEDIST");

// Network settings
int nDevices = 1;
int nGateways = 1;
double radius = 7500;
double simulationTime = 6400;
int pktsize=50;
bool same_position = true; 

// Channel model
bool realisticChannelModel = true;

//output filename for global perfomance
std::string filename="globalPerformance";

int appPeriodSeconds = 1000;
int transientPeriods = 0;
// by default capture 
bool capture =1;
// same rssi implem 
int captureState=1;
/*  enum DIST {
    SAME,
    UNIFORM,
    RANDOM,
    OTHER
  };*/
//DIST gwdist=DIST::SAME;
int gwdist=0;
int main (int argc, char *argv[])
{

  CommandLine cmd;
  cmd.AddValue ("nDevices", "Number of end devices to include in the simulation", nDevices);
  cmd.AddValue ("nGateways", "Number of gateways in the simulation", nGateways);
  cmd.AddValue ("radius", "The radius of the area to simulate", radius);
  cmd.AddValue ("simulationTime", "The time for which to simulate", simulationTime);
  cmd.AddValue ("appPeriod",
                "The period in seconds to be used by periodically transmitting applications",
                appPeriodSeconds);
  cmd.AddValue ("pktsize","packet size",pktsize);
  cmd.AddValue("same_position","all nodes are in the same postion",same_position);
  cmd.AddValue("fiName",
               "file name for periodic global performance output",
               filename );
cmd.AddValue("capture","default capture else aloha",capture); // 0 FOR aloha and 1 for goursaud and max 
cmd.AddValue("capState","default capture else aloha",captureState);             
cmd.AddValue ("gwdist", "The distribution of the gateways", gwdist); 
  cmd.Parse (argc, argv);

  // Set up logging
//LogComponentEnable ("NetworkThroughput", LOG_LEVEL_ALL);
//LogComponentEnable("LoraInterferenceHelper",LOG_LEVEL_ALL);
  // Make all devices use SF7 (i.e., DR5)
  Config::SetDefault ("ns3::EndDeviceLorawanMac::DataRate", UintegerValue (0));
 // to choose aloha(0 ) or default max capture
if (!capture)   
  {
    LoraInterferenceHelper::collisionMatrix = LoraInterferenceHelper::ALOHA;
  }
  // captureState is enum 
  /*
  0: aloha
  1: Max
  2: SameRssi
  3: switch
  4: mim
  */
  switch (captureState)
     {
     case 0:
      LoraInterferenceHelper::collisionMatrix = LoraInterferenceHelper::ALOHA;// par deafult la valeur est goursaud 
      break;
     case 1:
      SimpleGatewayLoraPhy::captureState = SimpleGatewayLoraPhy::MAX; // je peux aussi commenter cette ligne puisque j'avais mis lla valuer par default a MAX
      break;
     case 2:
      SimpleGatewayLoraPhy::captureState = SimpleGatewayLoraPhy::SAMERSSI;
      break;
     case 3:
      SimpleGatewayLoraPhy::captureState = SimpleGatewayLoraPhy::SWITCH;
      break;
     case 4:
      SimpleGatewayLoraPhy::captureState = SimpleGatewayLoraPhy::MIM;
      break;
  }
  /***********
   *  Setup 
   ***********/

  // Create the time value from the period
  Time appPeriod = Seconds (appPeriodSeconds);

  // Mobility
  MobilityHelper mobility;
  /*mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                 "rho", DoubleValue(radius),
                                 "X", DoubleValue (0.0),
                                  "Y", DoubleValue (0.0));*/
  /*mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                               //  "Theta", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=6.2830]"),
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=7500]"),
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0));*/
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");

  /************************
   *  Create the channel  *
   ************************/

  // Create the lora channel object
  Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
  loss->SetPathLossExponent (3.76);
  loss->SetReference (1, 7.7);

  if (realisticChannelModel)
    {

      // Aggregate shadowing to the logdistance loss
       Ptr<NakagamiPropagationLossModel> nak = CreateObject<NakagamiPropagationLossModel> ();
         loss->SetNext(nak);
      // Create the smal scale rayleigh  fading 
     
     
      // Aggregate fading to logdistance loss
      //shadowing->SetNext(nak);
      
      // Add the effect to the channel propagation loss
      //Ptr<BuildingPenetrationLoss> buildingLoss = CreateObject<BuildingPenetrationLoss> ();

      //nak->SetNext (buildingLoss);
    }

  Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();

  Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);

  /************************
   *  Create the helpers  *
   ************************/

  // Create the LoraPhyHelper
  LoraPhyHelper phyHelper = LoraPhyHelper ();
  phyHelper.SetChannel (channel);

  // Create the LorawanMacHelper
  LorawanMacHelper macHelper = LorawanMacHelper ();
  macHelper.SetRegion (LorawanMacHelper::ALOHA);

  // Create the LoraHelper
  LoraHelper helper = LoraHelper ();
  helper.EnablePacketTracking (); // Output filename

  //Create the NetworkServerHelper
  NetworkServerHelper nsHelper = NetworkServerHelper ();

  //Create the ForwarderHelper
  ForwarderHelper forHelper = ForwarderHelper ();

  /************************
   *  Create End Devices  *
   ************************/

  // Create a set of nodes
  NodeContainer endDevices;
  endDevices.Create (nDevices);

//mobility model modification for rssi channel verification, assign a fix position to and end node and see the ouput!  
  Ptr<ListPositionAllocator> allocator1 = CreateObject<ListPositionAllocator> ();
 if (same_position)
 {
   allocator1->Add (Vector (radius, 0.0, 0));
   mobility.SetPositionAllocator (allocator1);
 }

  // Assign a mobility model to each node
  mobility.Install (endDevices); 

  // Make it so that nodes are at a certain height > 0
  for (NodeContainer::Iterator j = endDevices.Begin (); j != endDevices.End (); ++j)
    {
      Ptr<MobilityModel> mobility = (*j)->GetObject<MobilityModel> ();
      Vector position = mobility->GetPosition ();
      position.z = 1.2;
      //NS_LOG_UNCOND(position.x);
      mobility->SetPosition (position);
    }
  // Create the LoraNetDevices of the end devices
  uint8_t nwkId = 54;
  uint32_t nwkAddr = 1864;
  Ptr<LoraDeviceAddressGenerator> addrGen =
      CreateObject<LoraDeviceAddressGenerator> (nwkId, nwkAddr);

  // Create the LoraNetDevices of the end devices
  macHelper.SetAddressGenerator (addrGen);
  phyHelper.SetDeviceType (LoraPhyHelper::ED);
  macHelper.SetDeviceType (LorawanMacHelper::ED_A);
  helper.Install (phyHelper, macHelper, endDevices);

  // Now end devices are connected to the channel

  // Connect trace sources
  for (NodeContainer::Iterator j = endDevices.Begin (); j != endDevices.End (); ++j)
    {
      Ptr<Node> node = *j;
      Ptr<LoraNetDevice> loraNetDevice = node->GetDevice (0)->GetObject<LoraNetDevice> ();
      Ptr<LoraPhy> phy = loraNetDevice->GetPhy ();
    }

  /*********************
   *  Create Gateways  *
   *********************/

  // Create the gateway nodes (allocate them uniformely on the disc)
  NodeContainer gateways;
  gateways.Create (nGateways);
  switch (gwdist)
        {
        case 0:
        {Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
         allocator->Add (Vector (0.0, 0.0, 15.0));}
        break;
        case 1:
        {mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator", "rho", DoubleValue (radius),
                                   "X", DoubleValue (0.0), "Y", DoubleValue (0.0));}
        break;
        case 2:
        {mobility.SetPositionAllocator ("ns3::RandomDiscPositionAllocator",
                                 "Rho", StringValue ("ns3::UniformRandomVariable[Min=0|Max=7500]"),
                                 "X", DoubleValue (0.0),
                                 "Y", DoubleValue (0.0));}
        break;
 
       default:
      { Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
       allocator->Add (Vector (0.0, 0.0, 15.0));}
        break;
 }


    // Make it so that nodes are at a certain height > 0     
     mobility.Install (gateways);                      
   for (NodeContainer::Iterator j = gateways.Begin (); j != gateways.End (); ++j)
    {
      Ptr<MobilityModel> mobility = (*j)->GetObject<MobilityModel> ();
      Vector position = mobility->GetPosition ();
      position.z = 15;
      //NS_LOG_UNCOND(position.x);
      mobility->SetPosition (position);
      NS_LOG_UNCOND(position.x <<"," <<position.y<<","<< position.z);
    }
  // Create a netdevice for each gateway
  phyHelper.SetDeviceType (LoraPhyHelper::GW);
  macHelper.SetDeviceType (LorawanMacHelper::GW);
  helper.Install (phyHelper, macHelper, gateways);

  NS_LOG_DEBUG ("Completed configuration");
  //macHelper.SetSpreadingFactorsUp (endDevices, gateways, channel);
  /*********************************************
   *  Install applications on the end devices  *
   *********************************************/

  Time appStopTime = Seconds (simulationTime);
  PeriodicSenderHelper appHelper = PeriodicSenderHelper ();
  appHelper.SetPeriod (appPeriod);
  appHelper.SetPacketSize (pktsize);
  Ptr<RandomVariableStream> rv = CreateObjectWithAttributes<UniformRandomVariable> (
      "Min", DoubleValue (0), "Max", DoubleValue (10));
  ApplicationContainer appContainer = appHelper.Install (endDevices);

  appContainer.Start (Seconds (0));
  appContainer.Stop (appStopTime);

  /**************************
   *  Create Network Server  *
   ***************************/

  // Create the NS node
  NodeContainer networkServer;
  networkServer.Create (1);

  // Create a NS for the network
  nsHelper.SetEndDevices (endDevices);
  nsHelper.SetGateways (gateways);
  nsHelper.Install (networkServer);

  //Create a forwarder for each gateway
  forHelper.Install (gateways);
  //helper.EnablePeriodicDeviceStatusPrinting (endDevices, gateways, "nodeData.txt", Seconds(simulationTime));
  helper.EnablePrintDevicePosition( endDevices,"NodePosition.txt");
  ////////////////
  // Simulation //
  ////////////////

  Simulator::Stop (appStopTime + Hours (1));

  NS_LOG_INFO ("Running simulation...");
  Simulator::Run ();

  Simulator::Destroy ();

  /////////////////////////////
  // Print results to stdout //
  /////////////////////////////
  NS_LOG_INFO ("Computing performance metrics...");

  LoraPacketTracker &tracker = helper.GetPacketTracker ();

  std::cout << tracker.GetTotalTimeOnAir();

  std::cout << tracker.CountMacPacketsGlobally (Seconds (0), appStopTime + Hours (1))
            << std::endl;

  //int gw_id =gateways.Get(0)->GetId ();
std::cout << "sent | rec  | interfe  | NoResources  |  Undr_sensitivity | gwtrasmitting"
            << std::endl;
for (int i=0; i<nGateways; i++)
    {
      std::cout << "gateway " << i <<" : "<< tracker.PrintPhyPacketsPerGw(Seconds (0), appStopTime + Hours (1),gateways.Get(i)->GetId ())
            << std::endl;
    }

return 0;
}
